# Testing DOMContentLoaded metrics

1. Если закомментить код в хэндлере на DOMContentLoaded, то упадет метрика dclEventEnd - dclEventStart, она отражаем обработку хэндлеров.

2. Если закомментить остальные циклы, то будет менятся метрика domContentLoadedEventStart. 

3. В циклах большие значения, чтобы заметить изменения
4. Итого, метрика dclEventEnd - dclEventStart отвечает за хэндлеры на событие DOMContentLoaded. На момент domContentLoadedEventStart отработали блокирующие скрипты и defer скрипты. https://html.spec.whatwg.org/multipage/parsing.html#the-end
